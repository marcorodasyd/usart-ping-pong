/**
 * \file
 *
 * \brief Configuration header
 *
 * Copyright (c) 2017-2018 Microchip Technology Inc. and its subsidiaries.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Subject to your compliance with these terms, you may use Microchip
 * software and any derivatives exclusively with Microchip products.
 * It is your responsibility to comply with third party license terms applicable
 * to your use of third party software (including open source software) that
 * may accompany Microchip software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY,
 * AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT WILL MICROCHIP BE
 * LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL
 * LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE
 * SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE
 * POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT
 * ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY
 * RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
 * THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="https://www.microchip.com/support/">Microchip Support</a>
 */

#ifndef _USART_DMA_CHANNEL_CONFIG_H_INCLUDED
#define _USART_DMA_CHANNEL_CONFIG_H_INCLUDED

// <h> Basic Configuration

// <o> USART Receive DMA Channel Number <0-12>
// <i> DMA Channel Number for USART receive operation, the channel number selected should be within allowed DMA channel range and it (channel) should be configured accordingly from DMAC system driver configuration
// <id> USART_receive_channel_number
#ifndef CONF_USART_RECEIVE_DMA_CHANNEL
#define CONF_USART_RECEIVE_DMA_CHANNEL 0
#endif

// <o> USART Transmit DMA Channel Number <0-12>
// <i> DMA Channel Number for USART transmit operation, the channel number selected should be within allowed DMA channel range and it (channel) should be configured accordingly from DMAC system driver configuration
// <id> USART_transmit_channel_number
#ifndef CONF_USART_TRANSMIT_DMA_CHANNEL
#define CONF_USART_TRANSMIT_DMA_CHANNEL 1
#endif

// </h>

#endif /* _USART_DMA_CHANNEL_CONFIG_H_INCLUDED */
