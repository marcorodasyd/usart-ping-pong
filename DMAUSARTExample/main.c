/**
 * \file
 *
 * \brief Application implement
 *
 * Copyright (c) 2017-2018 Microchip Technology Inc. and its subsidiaries.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Subject to your compliance with these terms, you may use Microchip
 * software and any derivatives exclusively with Microchip products.
 * It is your responsibility to comply with third party license terms applicable
 * to your use of third party software (including open source software) that
 * may accompany Microchip software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY,
 * AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT WILL MICROCHIP BE
 * LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL
 * LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE
 * SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE
 * POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT
 * ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY
 * RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
 * THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="https://www.microchip.com/support/">Microchip Support</a>
 */

#include <atmel_start.h>
#include <hpl_dma.h>
#include "usart_dma_channel_config.h"
#include "xdmac.h"

/* Enable ICache and DCache */
//#define CONF_BOARD_ENABLE_CACHE_AT_INIT

/* Cache Enabled */
//#define CONF_BOARD_ENABLE_CACHE

/** Micro-block length for single transfer  */
#define MICROBLOCK_LEN              (6)

/** Block length for single transfer  */
#define BLOCK_LEN                   (1)

/** The buffer size for transfer  */
#define BUFFER_SIZE                 (18)

/* Buffer length to transfer/receive */
#define BUFFER_LEN                  (6)

volatile uint8_t transferComplete       = 0;
volatile uint8_t receiveComplete        = 0;
volatile uint8_t receiveBlockComplete   = 0;

static uint8_t  ub_rx_cnt = 0;
static uint32_t ub_index = 0;

static uint8_t  string[BUFFER_LEN];
static uint8_t  tc_buffer[65536];

static uint32_t tc_payload_len = 0;
static uint8_t added_bytes_to_tc = 0;

COMPILER_ALIGNED(8) static uint8_t dst_buf[MICROBLOCK_LEN];


/** Linked list descriptor */
COMPILER_WORD_ALIGNED static lld_view0 lld[2];

/** XDMA channel configuration. */
static xdmac_channel_config_t xdmac_channel_cfg;

/** XDMAC channel HW Interface number for USART1. */
#define USART1_RX_HW_INTERFACE_NUM      (0xA)


/* Retrive added bytes to TC Payload by CTF */
static uint8_t tc_rx_uB_len(void) {
    switch ( tc_payload_len % 6 ) {
        case 0: return 0; break;
        case 1: return 5; break;
        case 2: return 4; break;
        case 3: return 3; break;
        case 4: return 2; break;
        case 5: return 1; break;
    }
}

volatile bool tc_completion_flag;
 
/* callbacks */
static void dma_transfer_done_rx(struct _dma_resource *const resource) {
    
    ub_rx_cnt++; // Increments by one every uB received
    if (ub_rx_cnt == 1 ) {
        tc_payload_len = dst_buf[5] + 1;
        added_bytes_to_tc = tc_rx_uB_len();
        // Updated TC payload len
        tc_payload_len += added_bytes_to_tc;
    }

    memcpy(&tc_buffer[ub_index], dst_buf, sizeof(dst_buf));
    ub_index += (uint32_t)MICROBLOCK_LEN;
    
    /* RXed complete TC */
    if ( ub_rx_cnt == tc_payload_len/6 + MICROBLOCK_LEN/6 )  {
        tc_completion_flag = true;
        ub_rx_cnt           = 0;
        tc_payload_len      = 0;
        added_bytes_to_tc   = 0;
        ub_index            = 0;
    }

    //if (tc_payload_len)
    //if (receiveComplete && buf_flag == 2) {
        //receiveComplete = 0;
        //buf_flag = 0;
        //Configure_Channel_variable_rx();
        //_dma_enable_transaction(CONF_USART_RECEIVE_DMA_CHANNEL, false);
    //}
}

/* callbacks */
static void dma_transfer_block_done_rx(struct _dma_resource *const resource)
{
	receiveBlockComplete = 1;
}

static void dma_error_rx(struct _dma_resource *const resource)
{
	/* write error handling code here */
}

static void dma_transfer_done_tx(struct _dma_resource *const resource)
{
	transferComplete = true;
}

static void dma_error_tx(struct _dma_resource *const resource)
{
	/* write error handling code here */
}

/* register callbacks */
void register_dma_rx_callback(void)
{
	struct _dma_resource *resource_rx;
	_dma_get_channel_resource(&resource_rx, CONF_USART_RECEIVE_DMA_CHANNEL);
	resource_rx->dma_cb.transfer_done = dma_transfer_done_rx;
    resource_rx->dma_cb.uBlock_done   = dma_transfer_block_done_rx;
	resource_rx->dma_cb.error         = dma_error_rx;
}

void register_dma_tx_callback(void)
{
	struct _dma_resource *resource_tx;
	_dma_get_channel_resource(&resource_tx, CONF_USART_TRANSMIT_DMA_CHANNEL);
	resource_tx->dma_cb.transfer_done = dma_transfer_done_tx;
	resource_tx->dma_cb.error         = dma_error_tx;
}

/* USART RX channel configuration */
void Configure_Channel_rx()
{
	_dma_set_source_address(CONF_USART_RECEIVE_DMA_CHANNEL, (uint32_t) & (((Usart *)(USART_0.device.hw))->US_RHR));
	_dma_set_destination_address(CONF_USART_RECEIVE_DMA_CHANNEL, (uint32_t *)string);
    _dma_set_data_amount(CONF_USART_RECEIVE_DMA_CHANNEL, (uint32_t)BUFFER_LEN);
    
    
	hri_xdmac_write_CC_SAM_bf(XDMAC, CONF_USART_RECEIVE_DMA_CHANNEL, 0);
	hri_xdmac_write_CC_DAM_bf(XDMAC, CONF_USART_RECEIVE_DMA_CHANNEL, 1);

	/* callback */
	register_dma_rx_callback();

	/* Enable DMA transfer complete interrupt */
	_dma_set_irq_state(CONF_USART_RECEIVE_DMA_CHANNEL, DMA_TRANSFER_COMPLETE_CB, true);
}

/***************************************************************/
/* USART RX channel configuration for variable DMA buffer size */
/***************************************************************/
void Configure_Channel_variable_rx() {

    /* Initialize channel config */
    xdmac_channel_cfg.mbr_ubc = MICROBLOCK_LEN;
    xdmac_channel_cfg.mbr_sa = (uint32_t) & (((Usart *)(USART_0.device.hw))->US_RHR);
    
    xdmac_channel_cfg.mbr_da = (uint32_t)dst_buf;
    xdmac_channel_cfg.mbr_cfg =   XDMAC_CC_TYPE_PER_TRAN
    		                    | XDMAC_CC_MEMSET_NORMAL_MODE
                                | XDMAC_CC_MBSIZE_SINGLE
                                | XDMAC_CC_DWIDTH_BYTE
                                | XDMAC_CC_SIF_AHB_IF1
                                | XDMAC_CC_DIF_AHB_IF1
                                | XDMAC_CC_SAM_FIXED_AM
                                | XDMAC_CC_DAM_INCREMENTED_AM
                                | XDMAC_CC_PERID(USART1_RX_HW_INTERFACE_NUM);

    xdmac_channel_cfg.mbr_bc = BLOCK_LEN - 1; // Number of blocks to RX
    xdmac_channel_cfg.mbr_ds =  0;
    xdmac_channel_cfg.mbr_sus = 0;
    xdmac_channel_cfg.mbr_dus = 0;
    
    xdmac_configure_transfer(XDMAC, CONF_USART_RECEIVE_DMA_CHANNEL, &xdmac_channel_cfg);

    /* Initialize linked list descriptor */
    // List 0
    lld[0].mbr_nda =    (uint32_t)(&lld[1]);
    lld[0].mbr_ubc =    XDMAC_UBC_NVIEW_NDV0 |
                        XDMAC_UBC_NDE_FETCH_EN |
                        XDMAC_UBC_NSEN_UPDATED |
                        XDMAC_UBC_NDEN_UPDATED |
                        XDMAC_UBC_UBLEN(MICROBLOCK_LEN);
    lld[0].mbr_da =     (uint32_t)dst_buf;

    // List 1
    lld[1].mbr_nda =    (uint32_t)(&lld[0]);
    lld[1].mbr_ubc =    XDMAC_UBC_NVIEW_NDV0 | XDMAC_UBC_NDE_FETCH_EN | XDMAC_UBC_NDEN_UPDATED | XDMAC_UBC_UBLEN(MICROBLOCK_LEN);
    lld[1].mbr_da  =    (uint32_t)dst_buf;

    xdmac_channel_set_descriptor_control(   XDMAC, CONF_USART_RECEIVE_DMA_CHANNEL, XDMAC_CNDC_NDVIEW_NDV0 |
                                            XDMAC_CNDC_NDE_DSCR_FETCH_EN |
                                            XDMAC_CNDC_NDSUP_SRC_PARAMS_UPDATED |
                                            XDMAC_CNDC_NDDUP_DST_PARAMS_UPDATED);                                  
    xdmac_channel_set_descriptor_addr(XDMAC, CONF_USART_RECEIVE_DMA_CHANNEL, (uint32_t)(&lld[0]), 0);
    
    //hri_xdmac_write_CC_PERID_bf(XDMAC, CONF_USART_RECEIVE_DMA_CHANNEL, 0x0A);
    
    xdmac_enable_interrupt(XDMAC, CONF_USART_RECEIVE_DMA_CHANNEL);
    xdmac_channel_enable_interrupt(XDMAC, CONF_USART_RECEIVE_DMA_CHANNEL, XDMAC_CIE_BIE);

    /* callback */
    register_dma_rx_callback();

#ifdef CONF_BOARD_ENABLE_CACHE
	SCB_CleanDCache();
#endif
    
}

/* USART TX channel configuration */
void Configure_Channel_tx()
{
	_dma_set_source_address(CONF_USART_TRANSMIT_DMA_CHANNEL, (uint32_t *)string);
	_dma_set_destination_address(CONF_USART_TRANSMIT_DMA_CHANNEL,
	                             (uint32_t) & (((Usart *)(USART_0.device.hw))->US_THR));
	_dma_set_data_amount(CONF_USART_TRANSMIT_DMA_CHANNEL, (uint32_t)BUFFER_LEN);

	/* For transmit channel ( Software to Peripheral ==> Increment source address and constant destination address ) */
	hri_xdmac_write_CC_SAM_bf(XDMAC, CONF_USART_TRANSMIT_DMA_CHANNEL, 1);
	hri_xdmac_write_CC_DAM_bf(XDMAC, CONF_USART_TRANSMIT_DMA_CHANNEL, 0);

	/* callback */
	register_dma_tx_callback();

	/* Enable DMA transfer complete interrupt */
	_dma_set_irq_state(CONF_USART_TRANSMIT_DMA_CHANNEL, DMA_TRANSFER_COMPLETE_CB, true);
}


static uint32_t nbr_tc_cnt = 0;

/********/
/* MAIN */
/********/
int main(void) {
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();

	/* enable USART module */
	usart_sync_enable(&USART_0);

	/* Configure DMA channels */
	//Configure_Channel_rx();
    Configure_Channel_variable_rx();
	//Configure_Channel_tx();

    // Init tc_buffer with 0's
    memset(tc_buffer, 0, sizeof(tc_buffer));

	_dma_enable_transaction(CONF_USART_RECEIVE_DMA_CHANNEL, false);

	/* Replace with your application code */
	while (1) {
        if (tc_completion_flag) {
            nbr_tc_cnt++;
            tc_completion_flag = false;
        }
    }

    
    /* Replace with your application code */
    //while (1) {
        //if (transferComplete) {
		        ///* reset source and destination address before re-enabling the channel */
		        //_dma_set_source_address(CONF_USART_RECEIVE_DMA_CHANNEL,
		                                //(uint32_t) & (((Usart *)(USART_0.device.hw))->US_RHR));
		        //_dma_set_destination_address(CONF_USART_RECEIVE_DMA_CHANNEL, (uint32_t *)string);
		        //_dma_set_data_amount(CONF_USART_RECEIVE_DMA_CHANNEL, (uint32_t)BUFFER_LEN);
//
		        //_dma_enable_transaction(CONF_USART_RECEIVE_DMA_CHANNEL, false);
		        //transferComplete = 0;
        //}
        //if (receiveComplete) {
		        ///* reset source and destination address before re-enabling the channel */
		        //_dma_set_source_address(CONF_USART_TRANSMIT_DMA_CHANNEL, (uint32_t *)string);
		        //_dma_set_destination_address(CONF_USART_TRANSMIT_DMA_CHANNEL,
		                                     //(uint32_t) & (((Usart *)(USART_0.device.hw))->US_THR));
		        //_dma_set_data_amount(CONF_USART_TRANSMIT_DMA_CHANNEL, (uint32_t)BUFFER_LEN);
//
		        //_dma_enable_transaction(CONF_USART_TRANSMIT_DMA_CHANNEL, false);
		        //receiveComplete = 0;
        //}
    //}

}
