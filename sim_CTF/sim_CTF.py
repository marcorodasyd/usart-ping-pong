import serial

# Config UART
ser = serial.Serial('COM3', timeout=3)
ser.baudrate = 9600


def uart_tx(bytes):
	''' UART _TX '''
	# ser.write(msg_bytes)
	ser.write(bytes)

	print('sent bytes : ', end=" ")
	for i in range(len(bytes)):
		print("0x{:02x}".format(bytes[i]), end=" ")
	print("")

def uart_rx(size):
	''' UART _TX '''
	t = ser.read(size)
	rx = bytearray(t)

	print('received   : ', end=" ")
	for i in range(len(rx)):
		print("0x{:02x}".format(rx[i]), end=" ")

def form_byte_to_tx(bytes_in):
	ret_bytes = bytearray()
	for i in range(len(bytes_in)):
		ret_bytes.append(bytes_in[i])
	return ret_bytes

options = {	0 : 0,
			1 : 5,
			2 : 4,
			3 : 3,
			4 : 2,
			5 : 1,
}

# MAIN
if __name__ == "__main__":
	# tc_primary_header      = [0x1A, 0x0C, 0xC0, 0x00, 0x00, 0x08, 0x1A, 0x0C, 0xC0, 0x00, 0x00, 0x08]
	# tc_secondary_header  	 = [0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x7, 0x08, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x7, 0x08, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x7, 0x08, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x7, 0x08, 0x55, 0x55, 0x55, 0x55]
	# tc_primary_header   	 = [0x1A, 0x0C, 0xC0, 0x00, 0x00, 0x20]

	# TC[8,1]
	# tc_real_primary_header   = [0x1A, 0x0C, 0xC0, 0x00 ,0x00, 0x0A]
	# tc_real_secondary_header = [0x29, 0x08, 0x01, 0x00, 0x01, 0x63, 0x01, 0x01, 0x16, 0xDA, 0x98]

	# TC[3,5]
	tc_real_primary_header   = [0x1A, 0x0C, 0xC0, 0x00, 0x00, 0x08]
	tc_real_secondary_header = [0x29, 0x03, 0x05, 0x00, 0x01, 0x01, 0x00, 0xD6, 0x71]

	# tc_test    = [0x1A, 0x0C, 0xC0, 0x00, 0x00, 0x20, 0x00, 0x20, 0x00, 0x20]
	# tc_test2   = [0xca, 0xfe, 0xca, 0xfe, 0xca, 0xfe, 0xca, 0xfe, 0xca, 0xfe]
	# tc_primary_header   = [0x01, 0x02, 0x03, 0x04, 0x05, 0x06]
	# tc_primary_header    = [0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x7, 0x08, 0x09, 0x0A, 0x0B, 0x0C]
	# tc_secondary_header  = [0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x7, 0x08, 0x55, 0x55, 0x55, 0x55]
	# tc_payload		  = [0xDE, 0xAD, 0xDE, 0xAD, 0xDE, 0xAD]
	# tc_primary_header = [0xCA]
	# tc_test_header = [0x1a, 0x1b, 0xca, 0xfe]

	print('Len of tc_primary_header : ', len(tc_real_primary_header))
	print('Len of tc_secondary_header : ', len(tc_real_secondary_header))
	print('Len of tc_secondary_header in chunks of 6 bytes : ', len(tc_real_secondary_header) / 6)
	print('MOD (len(bytes), 6 bytes) : ', len(tc_real_secondary_header) % 6)

	bytes_to_add = options[(tc_real_primary_header[5]+1) % 6]
	print('Nbr of Bytes added to TC : ', bytes_to_add)

	for i in range(bytes_to_add):
		tc_real_secondary_header.append(0x55)

	print('Len of tc_secondary_header in chunks of 6 bytes : ', len(tc_real_secondary_header) / 6)
	print('Len of tc_secondary_header in bytes : ', len(tc_real_secondary_header) )

	# Sending TC[8,1]``
	for i in range(20):
		bytes_primary = form_byte_to_tx(tc_real_primary_header)
		bytes_secondary = form_byte_to_tx(tc_real_secondary_header)
		tc = bytes_primary + bytes_secondary
		uart_tx(tc)
		tc_real_primary_header[4] += 1







	# for i in range(20):
		# bytes = form_byte_to_tx(tc_primary_header)
		# uart_tx(bytes)
		# bytes = form_byte_to_tx(tc_secondary_header)
		# uart_tx(bytes)

	# for i in range(20):
		# bytes = form_byte_to_tx(tc_test)
		# uart_tx(bytes)
		# bytes = form_byte_to_tx(tc_test2)
		# uart_tx(bytes)

	# uart_tx(bytes)
	# uart_rx(len(bytes))
